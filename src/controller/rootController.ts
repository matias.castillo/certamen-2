import { Request, Response } from 'express'
import * as rootRepository from '../repository/rootRepository'

export const sendDeafultMessage = (request: Request, response: Response) => {
    response.json(rootRepository.getRootMessage())
}